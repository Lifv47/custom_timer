import chime
import functools
import time

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

bp = Blueprint('timer', __name__, url_prefix='/timer')

def timer(start, end, mode):
    cur = time.time()
    while (cur - start) < end:
        print(f'{mode}: {(cur - start):.2f}')
        time.sleep(0.5)
        cur = time.time()
    start = time.time()
    return start

def main(reps, work_time, rest_time, sets, break_time):
    start = time.time()

    for i in range(sets):
        print(len(range(sets)))
        for j in range(reps):
            print('WORK')
            start = timer(start=start, end=work_time, mode='work')
            print('REST')
            start = timer(start=start, end=rest_time, mode='rest')
        chime.success()
        print(f'set {i} done!')
        if (i < len(range(sets)) - 1):
            start = timer(start=start, end=break_time, mode='break')

@bp.route('/timer', methods=('GET', 'POST'))
def timer():
    if request.method == 'POST':
        work_time  = request.form['work_time']
        rest_time  = request.form['rest_time']
        reps       = request.form['reps']
        break_time = request.form['break_time']
        sets       = request.form['sets']
        error = None

        if not work_time or work_time < 0:
            error += 'A positive work time is required '
        if not rest_time or rest_time < 0:
            error += 'A positive rest time is required '
        if not reps or reps <= 0:
            error += 'At least one rep is required '
        if break_time < 0:
            error += 'Break time must be positive'

        if error is None:
#            try:
            # Do timer stuff here
            main(reps=reps, work_time=work_time, rest_time=rest_time, sets=sets, break_time=break_time)
#        else:
#            return redirect(url_for("index")) # TODO jh: make sure this is right

        flash(error)
    return render_template('timer/timer.html')

